import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { Button, FlatList, StyleSheet, Text, View } from 'react-native';
import { GoalInput } from './components/GoalInput/GoalInput';
import { GoalItem } from './components/GoalItem/GoalItem';

type Object = {
	text: string;
	id: string;
};

export default function App() {
	const [ goalList, setGoalList ] = useState<Object[]>([]);
	const [ ModalVisibility, setModalVisibility ] = useState(false);

	const addGoalHandler = (enteredGoal: string) => {
		// setGoalList([ ...goalList, goal ]);
		setGoalList((previous) => [ ...previous, { text: enteredGoal, id: Math.random().toString() } ]);
	};

	const deleteGoalHandler = (id: string) => {
		setGoalList((previousList) => {
			return previousList.filter((goal) => goal.id !== id);
		});
	};

	const changeModalVisibility = () => {
		setModalVisibility((prev) => !prev);
	};
	return (
		<React.Fragment>
			<StatusBar style="light" />
			<View style={styles.container}>
				<Button title="Add New Goal" onPress={changeModalVisibility} color="#3c096c" />
				<GoalInput
					visible={ModalVisibility}
					changeVisibility={changeModalVisibility}
					addGoalHandler={addGoalHandler}
				/>

				<View style={styles.goalContainer}>
					<Text style={styles.goalListHeading}>List of Goals</Text>

					{/* <ScrollView>
					{goalList.map((goal: string, index: number) => (
						//To keep the design for both IOS and android you need to wrap in <View></View>
						// and apply the goalListItem style to this view component
						// some properties like COlor wont be applied as it is for Text element
						<Text style={styles.goalListItem} key={index}>
							{goal}
						</Text>
					))}
				</ScrollView> */}
					<FlatList
						data={goalList}
						renderItem={(itemData) => {
							return (
								<GoalItem
									text={itemData.item.text}
									id={itemData.item.id}
									onDeleteItem={deleteGoalHandler}
								/>
							);
						}}
						// If there is no key in the data provided, we can extract it from some values from the data object
						keyExtractor={(item, index) => {
							return item.id;
						}}
					/>
				</View>
			</View>
		</React.Fragment>
	);
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: '#be95c4',
		paddingTop: 50,
		paddingHorizontal: 26,
		flex: 1
	},

	goalContainer: {
		flex: 6
	},
	goalListHeading: {
		textAlign: 'center',
		fontWeight: 'bold',
		marginTop: 20,
		color: '#fff'
	}
});
