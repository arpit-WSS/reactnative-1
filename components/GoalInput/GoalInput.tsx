import { View, TextInput, Button, StyleSheet, Modal } from 'react-native';
import { useState } from 'react';

export const GoalInput = (props: {
	addGoalHandler: (enteredGoal: string) => void;
	visible: boolean;
	changeVisibility: () => void;
}) => {
	const [ entergoal, setgoal ] = useState<string>('');

	const goalInputChange = (enteredText: string) => {
		setgoal(enteredText);
	};
	const addGoal = () => {
		props.addGoalHandler(entergoal);
		props.changeVisibility();
		setgoal('');
	};
	return (
		<Modal visible={props.visible} animationType="fade">
			<View style={styles.inputContainer}>
				<TextInput
					style={styles.textInput}
					value={entergoal}
					onChangeText={goalInputChange}
					placeholder="Enter Your Goal"
				/>
				<View style={styles.buttonContainer}>
					<View style={styles.button}>
						<Button title="Cancel" color="#3c096c" onPress={props.changeVisibility} />
					</View>
					<View style={styles.button}>
						<Button color={'#3c096c'} onPress={addGoal} title="Add Goal" />
					</View>
				</View>
			</View>
		</Modal>
	);
};

const styles = StyleSheet.create({
	inputContainer: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
		marginBottom: 20,

		backgroundColor: '#be95c4'
	},
	textInput: {
		padding: 5,
		width: '85%',

		borderWidth: 1,
		borderRadius: 6,
		color: 'white',
		paddingLeft: 10,
		borderColor: '#fff',
		backgroundColor: '#9163cb'
	},
	buttonContainer: {
		flexDirection: 'row'
	},
	button: {
		marginTop: 15,
		width: '30%',
		marginHorizontal: 8
	}
});
