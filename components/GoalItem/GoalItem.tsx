import React from 'react';
import { View, Text, StyleSheet, Pressable } from 'react-native';

export const GoalItem = (props: { text: string; id: string; onDeleteItem: (id: string) => void }) => {
	return (
		<View style={styles.goalListItem}>
			<Pressable
				android_ripple={{ color: '#2c0c57' }}
				onPress={props.onDeleteItem.bind(this, props.id)}
				style={({ pressed }) => pressed && styles.iosPressedItem}
			>
				<Text style={styles.goalText}>{props.text}</Text>
			</Pressable>
		</View>
	);
};

const styles = StyleSheet.create({
	goalListItem: {
		margin: 5,
		borderRadius: 6,
		backgroundColor: '#5a189a'
	},
	iosPressedItem: {
		opacity: 0.5
	},
	goalText: {
		padding: 8,
		color: 'white'
	}
});
